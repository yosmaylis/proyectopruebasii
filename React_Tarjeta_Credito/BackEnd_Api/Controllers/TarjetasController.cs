﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BackEnd_Api.Models;

namespace BackEnd_Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TarjetasController : ControllerBase
    {
        private readonly TarjetaCreditoContext _context;

        public TarjetasController(TarjetaCreditoContext context)
        {
            _context = context;
        }

        // GET: api/Tarjetas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tarjetum>>> GetTarjeta()
        {
          if (_context.Tarjeta == null)
          {
              return NotFound();
          }
            return await _context.Tarjeta.ToListAsync();
        }

        // GET: api/Tarjetas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Tarjetum>> GetTarjetum(int id)
        {
          if (_context.Tarjeta == null)
          {
              return NotFound();
          }
            var tarjetum = await _context.Tarjeta.FindAsync(id);

            if (tarjetum == null)
            {
                return NotFound();
            }

            return tarjetum;
        }

        // PUT: api/Tarjetas/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTarjetum(int id, Tarjetum tarjetum)
        {
            if (id != tarjetum.Id)
            {
                return BadRequest();
            }

            _context.Entry(tarjetum).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TarjetumExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tarjetas
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Route("Guardar")]
        public async Task<ActionResult<Tarjetum>> PostTarjetum(Tarjetum tarjetum)
        {
          if (_context.Tarjeta == null)
          {
              return Problem("Entity set 'TarjetaCreditoContext.Tarjeta'  is null.");
          }
            _context.Tarjeta.Add(tarjetum);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTarjetum", new { id = tarjetum.Id }, tarjetum);
        }

        // DELETE: api/Tarjetas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTarjetum(int id)
        {
            if (_context.Tarjeta == null)
            {
                return NotFound();
            }
            var tarjetum = await _context.Tarjeta.FindAsync(id);
            if (tarjetum == null)
            {
                return NotFound();
            }

            _context.Tarjeta.Remove(tarjetum);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TarjetumExists(int id)
        {
            return (_context.Tarjeta?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
