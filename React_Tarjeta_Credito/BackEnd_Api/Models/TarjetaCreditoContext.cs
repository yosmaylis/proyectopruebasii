﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BackEnd_Api.Models
{
    public partial class TarjetaCreditoContext : DbContext
    {
        public TarjetaCreditoContext()
        {
        }

        public TarjetaCreditoContext(DbContextOptions<TarjetaCreditoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Tarjetum> Tarjeta { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=TarjetaCredito; Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tarjetum>(entity =>
            {
                entity.ToTable("tarjeta");

                entity.Property(e => e.Cvv).HasColumnName("CVV");

                entity.Property(e => e.FechaVencimiento)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("fecha_vencimiento");

                entity.Property(e => e.NombreTitular)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Nombre_Titular");

                entity.Property(e => e.NumeroTarjeta).HasColumnName("Numero_Tarjeta");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
