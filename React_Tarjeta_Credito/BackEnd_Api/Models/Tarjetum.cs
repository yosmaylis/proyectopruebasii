﻿using System;
using System.Collections.Generic;

namespace BackEnd_Api.Models
{
    public partial class Tarjetum
    {
        public int Id { get; set; }
        public long? NumeroTarjeta { get; set; }
        public string? FechaVencimiento { get; set; }
        public string? NombreTitular { get; set; }
        public int? Cvv { get; set; }
    }
}
